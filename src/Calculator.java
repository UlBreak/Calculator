import java.util.Scanner;


public class Calculator {

    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        double num1 = getNum();
        double num2 = getNum();
        char operation = getOperationSymbol();
        double result = performingOperation(num1,num2,operation);
        System.out.print("This is result: "+result);
    }


        protected static double getNum(){
            System.out.print("Enter number: ");
            double number;
            if(scanner.hasNextDouble())
                return scanner.nextDouble();
            else {
                System.out.println("This is not number");
                scanner.next();
                number = getNum();
            }
            return number;
        }

        protected static char getOperationSymbol() {
            System.out.print("Enter operation: ");
            char symbol;
            if(scanner.hasNext()){
                symbol = scanner.next().charAt(0);
            } else {
                System.out.println("Error. Try again!");
                scanner.next();
                symbol = getOperationSymbol();
            }
            return symbol;
        }


protected static double performingOperation(double firstVariable,double secondVariable,char symbol) {
    double result;
    switch (symbol) {
        case '+' -> result = firstVariable + secondVariable;
        case '-' -> result = firstVariable - secondVariable;
        case '*' -> result = firstVariable * secondVariable;
        case '/' -> result = firstVariable / secondVariable;
        case '%' -> result = firstVariable % secondVariable;
        default -> {
            System.out.print("Operation not recognized. Try again!");
            result = performingOperation(firstVariable, secondVariable, getOperationSymbol());
        }
    }
    return result;
}

    }



